This is the environment image for Docker containers on http://openshift.cern.ch to be able to run Flask and pytimber. The Python version is **2.7** due to compatibility reasons.

> PS! Due to the jpype package, the container exhibits memory leaking problems that are due to JDK8. To fix the problem in the future JDK > 10 should be used.

## Building the Docker image

- Clone this repository and navigate to the namonitor_docker folder. 
- Build the image with
	> docker build -t [image_name] .
