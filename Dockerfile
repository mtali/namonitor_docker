FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

RUN yum -y install java-1.8.0-openjdk-devel python-pip


RUN yum -y groupinstall "Development Tools" 

RUN yum -y install python-devel

COPY requirements.txt .
RUN pip install -r requirements.txt
RUN python -m cmmnbuild_dep_manager install pytimber 